module.exports = {
  apiURL: process.env.API_URL || "http://localhost:3001",
  title: {
    event: "<%= event.title %>",
  },
  meta: [
    {charset: "utf-8"},
    {name: "viewport", content: "width=device-width, initial-scale=1"},
    {hid: "description", name: "description", content: "A simple FCFS event slot assignment tool."},
  ],
};
