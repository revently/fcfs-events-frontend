const config = require("./config.js");

module.exports = {
  env: {
    apiURL: config.apiURL || process.env.API_URL || "http://localhost:3001",
    eventTitle: config.title && config.title.event || "<%= event.title %>",
    meta: JSON.stringify(config.meta || [
      {charset: "utf-8"},
      {name: "viewport", content: "width=device-width, initial-scale=1"},
      {hid: "description", name: "description", content: "A simple FCFS event slot assignment tool."},
    ]),
  },
  /*
  ** Default head of pages
  */
  head: {
    link: [
      // { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
    ],
  },
  css: [
    "@/assets/css/main.scss"
  ],
  /*
  ** Customize the progress bar color
  */
  loading: {color: "#3B8070"},
  /*
  ** Build configuration
  */
  build: {
    vendor: ["axios"],
    /*
    ** Run ESLint on save
    */
    extend(config, {isDev, isClient}) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    },
  },
};
